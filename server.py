#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import random


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        self.wfile.write(b"Hemos recibido tu peticion ")

        line = self.rfile.read()
        metodos = line.decode('utf-8').split(' ')[0]
        print(metodos)
        PORT = str(sys.argv[2])
        LINEA = line.decode('utf-8').split(' ')[2]
        USER = LINEA.split(':')[0]
        print("El cliente nos manda " + line.decode('utf-8'))

        if metodos == 'INVITE':
            self.wfile.write(b"\r\nContent-Type: application/sdp\r\n")
            self.wfile.write(b"SIP/2.0 100 Trying...\r\n")
            self.wfile.write(b"SIP/2.0 100 Ringing...\r\n")
            self.wfile.write(b"SIP/2.0 200 OK\r\n")
            self.wfile.write(b"v = 0\r\n")
            self.wfile.write(b"o = " + bytes(USER, 'utf-8') + b"\r\n")
            self.wfile.write(b"s = sesion Lourdes\r\n")
            self.wfile.write(b"t = 0\r\n")
            self.wfile.write(b"m = audio " + bytes(PORT, 'utf-8') + b" RTP\r\n")
        elif metodos == 'ACK':
            ALEAT = random.randint(1, 11111)
            RTP_Cabecera = simplertp.RtpHeader()
            RTP_Cabecera.set_header(version=2, pad_flag=0, ext_flag=0, cc=0,
                                    marker=0, payload_type=14, ssrc=ALEAT)
            audio = simplertp.RtpPayloadMp3(audio_file)
            simplertp.send_rtp_packet(RTP_Cabecera, audio, "127.0.0.1", 23032)

        elif metodos == 'BYE':
            self.wfile.write(b"SIP/2.0 200 OK\r\n")
        else:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n")


if __name__ == "__main__":
    """
    Programa principal
    """

    if len(sys.argv) != 4:
        sys.exit('Usage: pyhton3 server.py IP port audio:file')

    IP = sys.argv[1]
    PORT = int(sys.argv[2])
    audio_file = sys.argv[3]
    serv = socketserver.UDPServer((IP, PORT), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    print("Listening...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Servidor finalizado")
