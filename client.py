#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

# Cliente UDP simple.

# Dirección IP del servidor.
METODO = sys.argv[1]
direccion = sys.argv[2].split('@')
IP = direccion[1].split(':')[0]
PORT = int(sys.argv[2].split(':')[1])
RECEPTOR = sys.argv[2].split(':')[0]

# Contenido que vamos a enviar
LINE = METODO + ' sip:' + RECEPTOR + ' SIP/2.0\r\n'

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((IP, PORT))

    print("Enviando: " + LINE)
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    request = data.decode('utf-8')
    print(request)
    if METODO == 'INVITE':
        print("Content-Type: application/sdp")
        newLine = "ACK" + " sip: " + RECEPTOR + ' SIP/2.0\r\n'
        my_socket.send(bytes(newLine, 'utf-8'))
        print(newLine)
        print("v = 0\r\n")
        print("o = " + str(direccion) + IP + "\r\n")
        print("s = sesion Lourdes\r\n")
        print("t = 0\r\n")
        print("m = audio " + str(PORT) + " RTP\r\n")
    if METODO == 'BYE':
        print("Terminando socket...")

print("Fin.")
